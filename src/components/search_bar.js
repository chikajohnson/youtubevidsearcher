import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class SearchBar extends Component {
    constructor(props){
        super(props);

        this.onInputChange = this.onInputChange.bind(this);
        this.state = {term: ''};
    }

    
    onInputChange(event){
       this.setState({term : event.target.value });
    }

    render() { 
        return ( 
            <div>
                <input onChange={this.onInputChange} placeholder="I love react" />
                <div>value of the input: {this.state.term}</div>
            </div>
         );
    }

}
 
export default SearchBar;