import React from 'react';
import ReactDOM from 'react-dom';

import SearchBar from './components/search_bar';

const API_KEY = "AIzaSyCBX2dfW86YE6-84AIg45vTUV4i7QqHa5I";

const App = () => {	
	return	(
		<div>
			<SearchBar />
		</div>
	);
}

ReactDOM.render(<App />, document.querySelector(".container"));